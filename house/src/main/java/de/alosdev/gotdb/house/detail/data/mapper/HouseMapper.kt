package de.alosdev.gotdb.house.detail.data.mapper

import de.alosdev.gotdb.house.detail.api.response.HouseResponse
import de.alosdev.gotdb.house.detail.data.House

class HouseMapper {
    fun map(
        input: HouseResponse
    ): House {
        return House(
            name = input.name,
            region = input.region,
            coatOfArms = input.coatOfArms,
            seats = input.seats,
            titles = input.titles,
            words = input.words
        )
    }
}
