package de.alosdev.gotdb.house.detail.repository

import androidx.lifecycle.MutableLiveData
import de.alosdev.gotdb.core.coroutine.AppCoroutineDispatchers
import de.alosdev.gotdb.house.detail.api.HouseDetailApi
import de.alosdev.gotdb.house.detail.data.House
import de.alosdev.gotdb.house.detail.data.mapper.HouseMapper
import de.alosdev.gotdb.http.data.Failure
import de.alosdev.gotdb.http.data.Loading
import de.alosdev.gotdb.http.data.Resource
import de.alosdev.gotdb.http.data.Success
import kotlinx.coroutines.withContext
import timber.log.Timber

interface HouseDetailRepository {
    val houseResource: MutableLiveData<Resource<House>>
    suspend fun loadHouse()
}

class DefaultHouseDetailRepository(
    private val dispatchers: AppCoroutineDispatchers,
    private val api: HouseDetailApi,
    private val mapper: HouseMapper,
    private val houseId: Int
) : HouseDetailRepository {
    override val houseResource = MutableLiveData<Resource<House>>()

    override suspend fun loadHouse() {
        houseResource.postValue(Loading)
        withContext(dispatchers.network) {
            try {
                val response = api.fetchHouse(houseId).await()
                if (response.isSuccessful) {
                    houseResource.postValue(
                        Success(
                            mapper.map(
                                input = response.body()!!
                            )
                        )
                    )
                } else {
                    houseResource.postValue(Failure(response.code(), response.message()))
                }
            } catch (exception: Exception) {
                Timber.e(exception, "cannot load house list")
                houseResource.postValue(Failure(-1, "cannot load house list"))
            }
        }
    }
}
