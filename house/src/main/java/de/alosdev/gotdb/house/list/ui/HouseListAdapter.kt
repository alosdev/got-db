package de.alosdev.gotdb.house.list.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import de.alosdev.gotdb.house.R
import de.alosdev.gotdb.house.list.data.House

class HouseListAdapter(private val listener: RecyclerViewClickListener) : ListAdapter<House, HouseViewHolder>(HouseDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HouseViewHolder {
        return HouseViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_house, parent, false),
            listener
        )
    }

    override fun onBindViewHolder(holder: HouseViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class HouseViewHolder(view: View, private val listener: RecyclerViewClickListener) : RecyclerView.ViewHolder(view), View.OnClickListener {
    override fun onClick(v: View?) {
        listener.onClick(adapterPosition)
    }

    fun bind(item: House) {
        itemView.findViewById<TextView>(R.id.house_list_name).text = item.name
        itemView.findViewById<TextView>(R.id.house_list_region).text = item.region
        itemView.setOnClickListener(this@HouseViewHolder)
    }
}

class HouseDiff : DiffUtil.ItemCallback<House>() {
    override fun areItemsTheSame(oldItem: House, newItem: House): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: House, newItem: House): Boolean {
        return oldItem == newItem
    }
}
