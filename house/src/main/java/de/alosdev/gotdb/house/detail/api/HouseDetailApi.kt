package de.alosdev.gotdb.house.detail.api

import de.alosdev.gotdb.house.detail.api.response.HouseResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface HouseDetailApi {
    @GET("houses/{house_id}")
    fun fetchHouse(
        @Path("house_id") id: Int): Deferred<Response<HouseResponse>>
}
