package de.alosdev.gotdb.house.detail.ui

import android.os.Bundle
import de.alosdev.gotdb.core.ui.ToolbarProvider
import de.alosdev.gotdb.core.ui.mvvm.MvvmFragment
import de.alosdev.gotdb.house.R
import de.alosdev.gotdb.house.databinding.FragmentHouseDetailBinding
import org.koin.core.parameter.ParameterDefinition
import org.koin.core.parameter.parametersOf

class HouseDetailFragment : MvvmFragment<FragmentHouseDetailBinding, HouseDetailViewModel>(HouseDetailViewModel::class) {

    override fun getParameters(): ParameterDefinition {
        return {
            val args = HouseDetailFragmentArgs.fromBundle(arguments)
            parametersOf(args.id, args.name)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val parent = activity
        if (parent is ToolbarProvider) parent.getToolbar().title = viewModel.title
    }

    override fun getLayoutId(): Int = R.layout.fragment_house_detail
}
