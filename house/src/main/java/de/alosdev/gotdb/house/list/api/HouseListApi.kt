package de.alosdev.gotdb.house.list.api

import de.alosdev.gotdb.house.list.api.response.HouseResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface HouseListApi {
    @GET("houses")
    fun fetchPage(
        //  the page index starts with 1
        @Query("page") page: Int = 1,
        @Query("pageSize") pageSize: Int = 20
    ): Deferred<Response<List<HouseResponse>>>
}
