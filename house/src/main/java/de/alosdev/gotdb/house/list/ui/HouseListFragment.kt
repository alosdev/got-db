package de.alosdev.gotdb.house.list.ui

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.alosdev.gotdb.core.ui.mvvm.MvvmFragment
import de.alosdev.gotdb.house.R
import de.alosdev.gotdb.house.databinding.FragmentHouseListBinding
import timber.log.Timber

class HouseListFragment
    : MvvmFragment<FragmentHouseListBinding, HouseListViewModel>(HouseListViewModel::class) {
    private val scrollListener = PagingListener()

    override fun getLayoutId(): Int = R.layout.fragment_house_list

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initRecyclerView()

        viewModel.action.observe(this, Observer {
            when (it) {
                is HouseListViewModel.HouseSelected -> {
                    val action = HouseListFragmentDirections.detailAction(it.house.id, it.house.name)
                    findNavController().navigate(action)
                }
            }
        })
    }

    private fun initRecyclerView() {
        val listener: RecyclerViewClickListener = object : RecyclerViewClickListener {
            override fun onClick(position: Int) {
                viewModel.onItemClick(position)
            }
        }
        val houseListAdapter = HouseListAdapter(listener)
        binding.houseList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = houseListAdapter
            scrollListener.vm = viewModel
            addOnScrollListener(scrollListener)
        }
        viewModel.houseList.observe(this, Observer {
            houseListAdapter.submitList(it.items)
            scrollListener.hasMorePages = it.hasMorePages
        })
    }
}

class PagingListener(var hasMorePages: Boolean = true, var vm: HouseListViewModel? = null) : RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val llm = recyclerView.layoutManager as LinearLayoutManager
        Timber.v("visible=%d; total=%d; first=%d; last=%s", llm.childCount, llm.itemCount, llm.findFirstVisibleItemPosition(), llm.findLastVisibleItemPosition())
        if (hasMorePages && llm.itemCount < llm.findLastVisibleItemPosition() + 10) {
            vm?.loadNextPage()
        }
    }
}

