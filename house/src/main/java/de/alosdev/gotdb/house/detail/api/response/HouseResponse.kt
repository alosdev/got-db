package de.alosdev.gotdb.house.detail.api.response

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HouseResponse(
    val name: String,
    val region: String,
    val coatOfArms: String,
    val words: String,
    val titles: List<String>,
    val seats: List<String>
)
