package de.alosdev.gotdb.house.detail.data

data class House(
    val name: String,
    val region: String,
    val coatOfArms: String,
    val words: String,
    val titles: List<String>,
    val seats: List<String>
)
