package de.alosdev.gotdb.house.list.data

data class House(
    val id: Int,
    val url: String,
    val name: String,
    val region: String
)
