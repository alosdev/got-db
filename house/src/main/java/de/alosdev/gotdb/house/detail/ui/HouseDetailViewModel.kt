package de.alosdev.gotdb.house.detail.ui

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations.map
import de.alosdev.gotdb.core.coroutine.AppCoroutineDispatchers
import de.alosdev.gotdb.core.ui.BaseViewModel
import de.alosdev.gotdb.core.ui.livedata.map
import de.alosdev.gotdb.house.detail.data.House
import de.alosdev.gotdb.house.detail.repository.HouseDetailRepository
import de.alosdev.gotdb.http.data.Loading
import de.alosdev.gotdb.http.data.Success
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HouseDetailViewModel(
    private val dispatchers: AppCoroutineDispatchers,
    private val repository: HouseDetailRepository,
    val title: String
) : BaseViewModel(dispatchers) {
    private val resource = repository.houseResource
    val showProgress: LiveData<Boolean> = map(resource) {
        it is Loading
    }
    val showContent: LiveData<Boolean> = map(resource) {
        it is Success
    }
    val house: LiveData<House> = map(resource, { it is Success }) {
        (it as Success).data
    }

    init {
        loadHouse()
    }

    @VisibleForTesting
    internal fun loadHouse() {
        launch {
            withContext(dispatchers.computation) {
                repository.loadHouse()
            }
        }
    }
}
