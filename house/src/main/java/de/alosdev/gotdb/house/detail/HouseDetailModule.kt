package de.alosdev.gotdb.house.detail

import de.alosdev.gotdb.house.detail.api.HouseDetailApi
import de.alosdev.gotdb.house.detail.data.mapper.HouseMapper
import de.alosdev.gotdb.house.detail.repository.DefaultHouseDetailRepository
import de.alosdev.gotdb.house.detail.repository.HouseDetailRepository
import de.alosdev.gotdb.house.detail.ui.HouseDetailViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module.module
import retrofit2.Retrofit

val houseDetailModule = module {
    viewModel { (id: Int, title: String) ->
        HouseDetailViewModel(
            dispatchers = get(),
            repository = get(
                parameters = { parametersOf(id) }
            ),
            title = title
        )
    }
    factory { (id: Int) ->
        DefaultHouseDetailRepository(
            dispatchers = get(),
            api = createHouseDetailApi(get()),
            mapper = HouseMapper(),
            houseId = id
        ) as HouseDetailRepository
    }
}

private fun createHouseDetailApi(retrofit: Retrofit): HouseDetailApi {
    return retrofit.create(HouseDetailApi::class.java)
}
