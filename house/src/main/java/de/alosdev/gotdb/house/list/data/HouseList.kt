package de.alosdev.gotdb.house.list.data

data class HouseList(
    val currentPage: Int,
    val hasMorePages: Boolean,
    val items: List<House>
) {
    fun copyAndPushPage(houseList: HouseList): HouseList {
        val newItems: MutableList<House> = items.toMutableList()
        newItems.addAll(houseList.items)
        return HouseList(houseList.currentPage, houseList.hasMorePages, newItems)
    }
}
