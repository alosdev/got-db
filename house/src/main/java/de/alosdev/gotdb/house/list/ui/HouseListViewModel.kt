package de.alosdev.gotdb.house.list.ui

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import de.alosdev.gotdb.core.coroutine.AppCoroutineDispatchers
import de.alosdev.gotdb.core.ui.BaseViewModel
import de.alosdev.gotdb.core.ui.livedata.map
import de.alosdev.gotdb.core.ui.mvvm.SingleLiveEvent
import de.alosdev.gotdb.core.ui.mvvm.ViewAction
import de.alosdev.gotdb.http.data.Loading
import de.alosdev.gotdb.http.data.Success
import de.alosdev.gotdb.house.list.data.House
import de.alosdev.gotdb.house.list.repository.HouseListRepository
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import androidx.lifecycle.Transformations.map as pureMap

class HouseListViewModel(
    private val dispatchers: AppCoroutineDispatchers,
    private val repository: HouseListRepository
) : BaseViewModel(dispatchers) {
    private val resource = repository.houseListResource

    init {
        refreshPage()
    }

    val showProgress: LiveData<Boolean> = pureMap(resource) {
        it is Loading
    }

    val showContent: LiveData<Boolean> = pureMap(resource) {
        it is Success
    }
    val houseList = map(resource, { it is Success }) {
        (it as Success).data
    }
    @VisibleForTesting
    internal var isNotLoading = true

    val action = SingleLiveEvent<ViewAction>()

    @VisibleForTesting
    internal fun refreshPage() {
        launch {
            withContext(dispatchers.computation) {
                repository.loadFirstPage()
            }
        }
    }

    fun onItemClick(position: Int) {
        action.postValue(HouseSelected(houseList.value!!.items[position]))
    }

    fun loadNextPage() {
        launch {
            try {
                if (resource.value !is Loading && isNotLoading) {
                    isNotLoading = false
                    repository.loadNextPage()
                }
            } finally {
                isNotLoading = true
            }
        }
    }

    data class HouseSelected(val house: House) : ViewAction()
}
