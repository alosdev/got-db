package de.alosdev.gotdb.house.list.api.response

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HouseResponse(
    val url: String,
    val name: String,
    val region: String
)
