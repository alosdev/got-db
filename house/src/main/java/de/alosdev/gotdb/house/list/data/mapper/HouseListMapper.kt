package de.alosdev.gotdb.house.list.data.mapper

import de.alosdev.gotdb.house.list.api.response.HouseResponse
import de.alosdev.gotdb.house.list.data.House
import de.alosdev.gotdb.house.list.data.HouseList
import de.alosdev.gotdb.http.data.LinkParser
import retrofit2.Response
import timber.log.Timber

class HouseListMapper(private val linkParser: LinkParser) {
    fun map(
        listResponse: Response<List<HouseResponse>>,
        page: Int
    ): HouseList {
        Timber.d(listResponse.toString())
        return HouseList(
            currentPage = page,
            hasMorePages = linkParser.hasNextPage(listResponse),
            items = listResponse.body()!!.map { houseResponse ->
                House(
                    id = houseResponse.url.substring(houseResponse.url.lastIndexOf('/') + 1).toInt(),
                    url = houseResponse.url,
                    name = houseResponse.name,
                    region = houseResponse.region
                )
            }
        )
    }
}
