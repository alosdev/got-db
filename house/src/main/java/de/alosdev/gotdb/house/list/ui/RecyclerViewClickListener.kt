package de.alosdev.gotdb.house.list.ui

interface RecyclerViewClickListener {
    fun onClick(position: Int)
}
