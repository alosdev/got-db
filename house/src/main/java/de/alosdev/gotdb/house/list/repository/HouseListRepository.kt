package de.alosdev.gotdb.house.list.repository

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import de.alosdev.gotdb.core.coroutine.AppCoroutineDispatchers
import de.alosdev.gotdb.house.list.api.HouseListApi
import de.alosdev.gotdb.house.list.data.HouseList
import de.alosdev.gotdb.house.list.data.mapper.HouseListMapper
import de.alosdev.gotdb.http.data.Failure
import de.alosdev.gotdb.http.data.Loading
import de.alosdev.gotdb.http.data.Resource
import de.alosdev.gotdb.http.data.Success
import kotlinx.coroutines.withContext
import timber.log.Timber

interface HouseListRepository {
    val houseListResource: MutableLiveData<Resource<HouseList>>
    suspend fun loadFirstPage()
    suspend fun loadNextPage()
}

class DefaultHouseListRepository(
    private val dispatchers: AppCoroutineDispatchers,
    private val api: HouseListApi,
    private val mapper: HouseListMapper
) : HouseListRepository {
    @VisibleForTesting
    internal val resource = MutableLiveData<Resource<HouseList>>()
    override val houseListResource = resource

    override suspend fun loadFirstPage() {
        withContext(dispatchers.network) {
            resource.postValue(Loading)
            try {
                val response = api.fetchPage().await()
                if (response.isSuccessful) {
                    resource.postValue(
                        Success(
                            mapper.map(
                                listResponse = response,
                                page = 1
                            )
                        )
                    )
                } else {
                    resource.postValue(Failure(response.code(), response.message()))
                }
            } catch (exception: Exception) {
                Timber.e(exception, "cannot load house list")
                resource.postValue(Failure(-1, "cannot load house list"))
            }
        }
    }

    override suspend fun loadNextPage() {
        withContext(dispatchers.network) {
            val oldResource = resource.value as Success
            val oldData = oldResource.data
            val page = oldData.currentPage + 1
            val response = api.fetchPage(page = page).await()
            if (response.isSuccessful) {
                resource.postValue(
                    Success(
                        oldData.copyAndPushPage(
                            mapper.map(
                                listResponse = response,
                                page = page
                            )
                        )
                    )
                )
            }
        }
    }
}
