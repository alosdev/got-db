package de.alosdev.gotdb.house.list

import de.alosdev.gotdb.house.list.api.HouseListApi
import de.alosdev.gotdb.house.list.data.mapper.HouseListMapper
import de.alosdev.gotdb.house.list.repository.DefaultHouseListRepository
import de.alosdev.gotdb.house.list.repository.HouseListRepository
import de.alosdev.gotdb.house.list.ui.HouseListViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit

val houseListModule = module {
    viewModel {
        HouseListViewModel(
            dispatchers = get(),
            repository = get()
        )
    }
    factory {
        DefaultHouseListRepository(
            dispatchers = get(),
            api = createHouseListApi(get()),
            mapper = HouseListMapper(get())
        ) as HouseListRepository
    }
}

private fun createHouseListApi(retrofit: Retrofit): HouseListApi {
    return retrofit.create(HouseListApi::class.java)
}
