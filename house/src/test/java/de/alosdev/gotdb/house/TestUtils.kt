package de.alosdev.gotdb.house

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import de.alosdev.gotdb.core.coroutine.AppCoroutineDispatchers
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers.Unconfined
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

object TestUtils {
    val testLifecycleOwner: LifecycleOwner = object : LifecycleOwner {

        private val registry = init()

        // Creates a LifecycleRegistry in RESUMED state.
        private fun init(): LifecycleRegistry {
            val registry = LifecycleRegistry(this)
            registry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
            registry.handleLifecycleEvent(Lifecycle.Event.ON_START)
            registry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
            return registry
        }

        override fun getLifecycle(): Lifecycle {
            return registry
        }
    }

    val dispatchers = AppCoroutineDispatchers(Unconfined, Unconfined, Unconfined)
}

fun <T> deferred(deferred: T): Deferred<T> {
    return GlobalScope.async(Unconfined) { deferred }
}
