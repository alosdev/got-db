package de.alosdev.gotdb.house.detail.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import de.alosdev.gotdb.house.TestUtils
import de.alosdev.gotdb.house.detail.repository.HouseDetailRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.reset
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HouseDetailViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var repository: HouseDetailRepository

    private lateinit var toBeTested: HouseDetailViewModel

    @Before
    fun setUp() {
        runBlocking {
            given(repository.houseResource).willReturn(MutableLiveData())
            toBeTested = HouseDetailViewModel(TestUtils.dispatchers, repository, "title")
            then(repository).should().loadHouse()
            reset(repository)
        }
    }

    @Test
    fun `check If repository get called`() {
        runBlocking {
            toBeTested.loadHouse()

            then(repository).should().loadHouse()
        }
    }
}
