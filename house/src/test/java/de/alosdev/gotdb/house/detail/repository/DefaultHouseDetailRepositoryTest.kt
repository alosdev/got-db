package de.alosdev.gotdb.house.detail.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import de.alosdev.gotdb.house.TestUtils
import de.alosdev.gotdb.house.deferred
import de.alosdev.gotdb.house.detail.api.HouseDetailApi
import de.alosdev.gotdb.house.detail.api.response.HouseResponse
import de.alosdev.gotdb.house.detail.data.House
import de.alosdev.gotdb.house.detail.data.mapper.HouseMapper
import de.alosdev.gotdb.http.data.Failure
import de.alosdev.gotdb.http.data.Success
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class DefaultHouseDetailRepositoryTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var api: HouseDetailApi

    private lateinit var toBeTested: HouseDetailRepository

    @Before
    fun setUp() {
        toBeTested = DefaultHouseDetailRepository(
            TestUtils.dispatchers,
            api,
            HouseMapper(),
            4711
        )
    }

    @Test
    fun `check house loading sucessful`() {
        runBlocking {
            given(api.fetchHouse(4711)).willReturn(deferred(Response.success(HouseResponse("name", "region", "coatOfArms", "word", listOf(), listOf()))))

            toBeTested.loadHouse()

            assertThat(toBeTested.houseResource.value).isEqualTo(Success(House("name", "region", "coatOfArms", "word", listOf(), listOf())))
        }
    }

    @Test
    fun `check house failed with exception while loading`() {
        runBlocking {
            given(api.fetchHouse(4711)).willThrow(RuntimeException())

            toBeTested.loadHouse()

            assertThat(toBeTested.houseResource.value).isEqualTo(Failure(-1, "cannot load house list"))
        }
    }

    @Test
    fun `check house failed with request not successful`() {
        runBlocking {
            given(api.fetchHouse(4711)).willReturn(deferred(Response.error<HouseResponse>(404, ResponseBody.create(null, byteArrayOf(0)))))

            toBeTested.loadHouse()

            assertThat(toBeTested.houseResource.value).isEqualTo(Failure(404, "Response.error()"))
        }
    }
}
