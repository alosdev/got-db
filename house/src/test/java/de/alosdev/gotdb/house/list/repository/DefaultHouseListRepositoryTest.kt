package de.alosdev.gotdb.house.list.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import de.alosdev.gotdb.house.TestUtils
import de.alosdev.gotdb.house.deferred
import de.alosdev.gotdb.house.list.api.HouseListApi
import de.alosdev.gotdb.house.list.api.response.HouseResponse
import de.alosdev.gotdb.house.list.data.House
import de.alosdev.gotdb.house.list.data.HouseList
import de.alosdev.gotdb.house.list.data.mapper.HouseListMapper
import de.alosdev.gotdb.http.data.Failure
import de.alosdev.gotdb.http.data.LinkParser
import de.alosdev.gotdb.http.data.Success
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class DefaultHouseListRepositoryTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var api: HouseListApi
    @Mock
    private lateinit var linkParser: LinkParser

    private lateinit var toBeTested: HouseListRepository

    @Before
    fun setUp() {
        toBeTested = DefaultHouseListRepository(
            TestUtils.dispatchers,
            api,
            HouseListMapper(linkParser)
        )
    }

    @Test
    fun checkMovieLoadingSuccessful() {
        runBlocking {
            given(api.fetchPage()).willReturn(deferred(Response.success(listOf(HouseResponse("url:/4711", "name", "region")))))

            toBeTested.loadFirstPage()

            assertThat(toBeTested.houseListResource.value).isEqualTo(Success(HouseList(1, false, listOf(House(4711, "url:/4711", "name", "region")))))
        }
    }

    @Test
    fun checkMovieFailedWithExceptionWhileLoading() {
        runBlocking {
            given(api.fetchPage()).willThrow(RuntimeException())

            toBeTested.loadFirstPage()

            assertThat(toBeTested.houseListResource.value).isEqualTo(Failure(-1, "cannot load house list"))
        }
    }

    @Test
    fun checkMovieFailedWithRequestNotSuccessful() {
        runBlocking {
            given(api.fetchPage()).willReturn(deferred(Response.error<List<HouseResponse>>(404, ResponseBody.create(null, byteArrayOf(0)))))

            toBeTested.loadFirstPage()

            assertThat(toBeTested.houseListResource.value).isEqualTo(Failure(404, "Response.error()"))
        }
    }

    @Test
    fun checkLoadingNextPage() {
        runBlocking {
            toBeTested.houseListResource.value = Success(HouseList(1, true, listOf(House(815, "url:/815", "name", "region"))))
            given(api.fetchPage(2)).willReturn(deferred(Response.success(listOf(HouseResponse("url:/4711", "name", "region")))))

            toBeTested.loadNextPage()

            assertThat(toBeTested.houseListResource.value).isEqualTo(Success(HouseList(2, false, listOf(House(815, "url:/815", "name", "region"), House(4711, "url:/4711", "name", "region")))))
        }
    }
}
