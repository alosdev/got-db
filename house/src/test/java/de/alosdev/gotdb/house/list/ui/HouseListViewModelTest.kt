package de.alosdev.gotdb.house.list.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.google.common.truth.Truth.assertThat
import de.alosdev.gotdb.house.TestUtils
import de.alosdev.gotdb.house.list.data.House
import de.alosdev.gotdb.house.list.data.HouseList
import de.alosdev.gotdb.house.list.repository.HouseListRepository
import de.alosdev.gotdb.http.data.Resource
import de.alosdev.gotdb.http.data.Success
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.reset
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HouseListViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var repository: HouseListRepository

    private lateinit var toBeTested: HouseListViewModel

    private val resource = MutableLiveData<Resource<HouseList>>()

    @Before
    fun setUp() {
        given(repository.houseListResource).willReturn(resource)
        toBeTested = HouseListViewModel(TestUtils.dispatchers, repository)
        reset(repository)
    }

    @Test
    fun checkLoadFirstPage() {
        runBlocking {

            toBeTested.refreshPage()

            then(repository).should().loadFirstPage()
        }
    }

    @Test
    fun checkLoadNextPage() {
        runBlocking {

            toBeTested.loadNextPage()

            then(repository).should().loadNextPage()
        }
    }

    @Test
    fun checkItemSelected() {
        runBlocking {
            toBeTested.houseList.observe(TestUtils.testLifecycleOwner, Observer { })
            val house = House(4711, "url", "name", "region")
            resource.postValue(Success(HouseList(1, false, listOf(house))))

            toBeTested.onItemClick(0)

            assertThat(toBeTested.action.value).isEqualTo(HouseListViewModel.HouseSelected(house))
        }
    }
}
