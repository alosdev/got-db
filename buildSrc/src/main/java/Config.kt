object Versions {

    val app_version_code = 1
    val app_version_name = "0.1"
    val appcompat = "1.0.2"
    val android_gradle_plugin = "3.3.2"
    val build_tools = "28.0.3"
    val compile_sdk = 28
    val constraint = "1.1.3"
    val coroutines = "1.0.1"
    val gradle = "4.10.3"
    val koin = "1.0.2"
    val kotlin = "1.3.11"
    val lifecycle = "2.0.0"
    val min_sdk = 21
    val moshi = "1.8.0"
    val navigation = "1.0.0-alpha08"
    val target_sdk = 28
}

object Deps {

    val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
    val constraint = "androidx.constraintlayout:constraintlayout:${Versions.constraint}"
    val coroutine_android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    val gradle_plugin_android = "com.android.tools.build:gradle:${Versions.android_gradle_plugin}"
    val gradle_plugin_depMgmt = "com.github.ben-manes:gradle-versions-plugin:0.20.0"
    val gradle_plugin_kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    val gradle_plugin_detekt = "gradle.plugin.io.gitlab.arturbosch.detekt:detekt-gradle-plugin:1.0.0.RC8"
    val koin_android = "org.koin:koin-android:${Versions.koin}"
    val koin_android_viewmodel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"
    val kotlin_stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    val kotlin_lint = "com.github.shyiko:ktlint:0.29.0"
    val material_components = "com.google.android.material:material:1.1.0-alpha01"
    val moshi = "com.squareup.moshi:moshi:${Versions.moshi}"
    val moshi_kotlin_codegen = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}"
    val navigation_fragment = "android.arch.navigation:navigation-fragment-ktx:${Versions.navigation}"
    val navigation_safeargs = "android.arch.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation}"
    val navigation_ui = "android.arch.navigation:navigation-ui-ktx:${Versions.navigation}"
    val navigation_test = "android.arch.navigation:navigation-testing-ktx:${Versions.navigation}"
    val ktx_core = "androidx.core:core-ktx:1.0.1"
    val lifecycle = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle}"
    val ok_http = "com.squareup.okhttp3:okhttp:3.12.0"
    val ok_http_logging = "com.squareup.okhttp3:logging-interceptor:3.12.0"
    val picasso = "com.squareup.picasso:picasso:2.71828"
    val retrofit = "com.squareup.retrofit2:retrofit:2.5.0"
    val retrofit_call_adapter = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:0.9.2"
    val retrofit_converter_moshi = "com.squareup.retrofit2:converter-moshi:2.5.0"
    val test_espresso_core = "androidx.test.espresso:espresso-core:3.1.0-alpha3"
    val test_espresso_runner = "androidx.test:runner:1.1.0-alpha3"
    val test_google_truth = "com.google.truth:truth:0.42"
    val test_junit = "junit:junit:4.12"
    val test_koin = "org.koin:koin-test:${Versions.koin}"
    val test_mockito = "org.mockito:mockito-core:2.23.4"
    val test_arch_core = "androidx.arch.core:core-testing:2.0.0"
    val timber = "com.jakewharton.timber:timber:4.7.1"

}
