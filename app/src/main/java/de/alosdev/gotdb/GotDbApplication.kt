package de.alosdev.gotdb

import android.app.Application
import de.alosdev.gotdb.core.coreModule
import de.alosdev.gotdb.house.detail.houseDetailModule
import de.alosdev.gotdb.house.list.houseListModule
import de.alosdev.gotdb.http.httpModule
import org.koin.android.ext.android.startKoin
import timber.log.Timber

class GotDbApplication : Application() {

    internal val moduleList = setOf(
        coreModule,
        httpModule,
        houseListModule,
        houseDetailModule
    )

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin(this, moduleList.toList())
    }
}
