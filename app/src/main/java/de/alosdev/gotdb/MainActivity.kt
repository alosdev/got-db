package de.alosdev.gotdb

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import de.alosdev.gotdb.core.ui.ToolbarProvider

class MainActivity : AppCompatActivity(), ToolbarProvider {
    private lateinit var toolBar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolBar = findViewById(R.id.tool_bar)
        setSupportActionBar(toolBar)
        NavigationUI.setupWithNavController(toolBar, findNavController(R.id.main_nav_fragment))
    }

    override fun getToolbar(): Toolbar = toolBar

    override fun onSupportNavigateUp() = findNavController(R.id.main_nav_fragment).navigateUp()
}
