package de.alosdev.gotdb.http.data

import com.google.common.truth.Truth.assertThat
import okhttp3.Headers
import org.junit.Test
import retrofit2.Response
import retrofit2.http.Header

class LinkParserTest {
    @Test
    fun `Check if returns false if no link header exist`() {
        val response = Response.success(Unit)
        assertThat(LinkParser().hasNextPage(response)).isFalse()
    }

    @Test
    fun `Check if returns false if link header with no next rel exist`() {
        val response = Response.success(
            Unit,
            Headers.Builder()
                .add(
                    "Link",
                    "<https://www.anapioficeandfire.com/api/houses?page=2&pageSize=20>; rel=\"whatever\", <https://www.anapioficeandfire.com/api/houses?page=1&pageSize=20>; rel=\"first\", <https://www.anapioficeandfire.com/api/houses?page=23&pageSize=20>; rel=\"last\""
                ).build()
        )
        assertThat(LinkParser().hasNextPage(response)).isFalse()
    }

    @Test
    fun `Check if returns true if link header with next rel exist`() {
        val response = Response.success(
            Unit,
            Headers.Builder()
                .add(
                    "Link",
                    "<https://www.anapioficeandfire.com/api/houses?page=2&pageSize=20>; rel=\"next\", <https://www.anapioficeandfire.com/api/houses?page=1&pageSize=20>; rel=\"first\", <https://www.anapioficeandfire.com/api/houses?page=23&pageSize=20>; rel=\"last\""
                ).build()
        )
        assertThat(LinkParser().hasNextPage(response)).isTrue()
    }
}
