package de.alosdev.gotdb.http.data

sealed class Resource<out T>

data class Success<out T>(val data: T) : Resource<T>()
object Loading : Resource<Nothing>()
data class Failure(val code: Int, val message: String) : Resource<Nothing>()
