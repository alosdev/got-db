package de.alosdev.gotdb.http.data

import okhttp3.Headers
import retrofit2.Response

private const val HEADER_KEY_LINK = "Link"

interface LinkParser {
    fun hasNextPage(response: Response<*>): Boolean
}

class DefaultLinkParser : LinkParser {
    // <https://www.anapioficeandfire.com/api/houses?page=2&pageSize=20>; rel="next",
    // <https://www.anapioficeandfire.com/api/houses?page=1&pageSize=20>; rel="first",
    // <https://www.anapioficeandfire.com/api/houses?page=23&pageSize=20>; rel="last"
    override fun hasNextPage(response: Response<*>): Boolean {
        return hasNextPage(response.headers())
    }

    private fun hasNextPage(headers: Headers): Boolean {
        val linkHeader = headers.get(HEADER_KEY_LINK) ?: return false
        return linkHeader
            .split(", ")
            .find {
                it.endsWith("; rel=\"next\"")
            }?.isNotBlank() ?: false
    }
}
