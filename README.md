Game of Thrones DB Application
==============================

This application provides the houses from Game of Thromes. You can
browse houses in a list.
You can select an item via clicking on the row. This brings you to
the detail view of the House.
Currently, there wasn't a big focus on the layout, which also means that
not all available data was shown. The application is written completely
in Kotlin and with the new androidx libraries. This application uses
Kotlin coroutines instead of RxJava.

## Libraries
Following the major application are listed, with a small description:
- appcompat with material-components (UI)
- koin (service locator instead of Dagger)
- retrofit with okHttp (HTTP stack with API calls)
- Moshi (JSON serializer, with code generation out of kotlin data classes)
- navigation (NavigationController provided by arch components)
- Timber (logging framework)

## Starting the application
Just run the application from the app module

## Architecture
For the presentation layer MVVM with data, binding is used as the base
architecture together with <code>LiveData</code>. The Main part of the architecture
uses the clean code without the extra layer with <code>Usecases</code>.
The state is held via LiveData in the repositories. The view models
just handle UI Logic whereas the business logic is handled in the
repositories.

## Modules
Several Modules are created:
- core
    - The base of all modules, where the default styles, themes are defined. Also, it contains base classes.
- http
    - Contains http configuration and retrofit definition.
    Here you can also change the API base URL in the build.gradle.
- house
    - Contains everything which relates to the list and details of the Game of Thrones API.
    Both parts of the houses are separated by different packages.
- app
    - Application definition

## Testing
Several unit tests are available. Mainly for Repositories and ViewModels.
Because of the limited time, no UI tests were created.

## Reflections
The API is easy to use. It would be nice to have some pictures, but probably because of licensing these weren't available.

## Extensibility
- Creating UI tests with Espresso
- Show more data on the house detail screen.
- Setting up a nice looking base theme
- Introduce splash screen
- Add deeplinking also with the possibility for auto verifying.

## based on following repositories
- https://gitlab.com/alosdev/movie-db
- if you want to see an application without libraries take a look here: https://gitlab.com/alosdev/HeyBeachTest
