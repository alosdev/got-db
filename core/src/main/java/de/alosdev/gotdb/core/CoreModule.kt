package de.alosdev.gotdb.core

import de.alosdev.gotdb.core.coroutine.AppCoroutineDispatchers
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import org.koin.dsl.module.module

val coreModule = module {
    single {
        AppCoroutineDispatchers(
            main = Main,
            network = IO,
            computation = Default
        )
    }
}
