package de.alosdev.gotdb.core.ui

import androidx.appcompat.widget.Toolbar

interface ToolbarProvider {
    fun getToolbar(): Toolbar
}
