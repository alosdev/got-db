package de.alosdev.gotdb.core.coroutine

import kotlin.coroutines.CoroutineContext

data class AppCoroutineDispatchers(
    val network: CoroutineContext,
    val main: CoroutineContext,
    val computation: CoroutineContext
)
