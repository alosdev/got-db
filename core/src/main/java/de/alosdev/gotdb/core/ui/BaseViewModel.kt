package de.alosdev.gotdb.core.ui

import androidx.annotation.CallSuper
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import de.alosdev.gotdb.core.coroutine.AppCoroutineDispatchers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel

abstract class BaseViewModel(private val dispatchers: AppCoroutineDispatchers) : ViewModel(), CoroutineScope {
    private var job = Job()

    override val coroutineContext = job + dispatchers.main

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        coroutineContext.cancel()
    }
}
